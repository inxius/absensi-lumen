<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserDetail;

class PegawaiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        try {
            $pegawai = DB::table('users')
                    ->join('user_details', 'users.id', '=', 'user_details.user_id')
                    ->select('users.id', 'users.email', 'user_details.nik', 'user_details.nama', 'user_details.tanggal_lahir', 'user_details.jk', 'user_details.alamat', 'user_details.jabatan', 'user_details.no_pegawai', 'user_details.no_bpjstk')
                    ->get();

            return response()->json([
                'message' => 'success',
                'data' => $pegawai,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Failed',
                'error' => $th,
            ], 400);
        }

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email', 'unique:users'],
            'nama' => ['required'],
            'nik' => ['required', 'min:15', 'max:18', 'unique:user_details,nik'],
            'tgl_lahir' => ['required', 'date'],
            'jk' => ['required'],
            'alamat' => ['required'],
            'jabatan' => ['required'],
            'no_pegawai' => ['required', 'unique:user_details,no_pegawai'],
            'bpjstk' => ['unique:user_details,no_bpjstk'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Error!',
                'error' => $validator->errors(),
            ], 400);
        }

        $pegawai =  $validator->validated();

        try {
            DB::beginTransaction();

            $password = Hash::make('Password');

            $user = User::create([
                'email' => $pegawai['email'],
                'password' => $password,
                'roles' => 2,
            ]);

            $userDetail = UserDetail::create([
                'user_id' => $user->id,
                'nik' => $pegawai['nik'],
                'nama' => $pegawai['nama'],
                'tanggal_lahir' => $pegawai['tgl_lahir'],
                'jk' => $pegawai['jk'],
                'alamat' => $pegawai['alamat'],
                'jabatan' => $pegawai['jabatan'],
                'no_pegawai' => $pegawai['no_pegawai'],
                'no_bpjstk' => $request['bpjstk'],
            ]);

            DB::commit();

            return response()->json([
                'message' => 'Success',
            ], 200);
        } catch (\Throwable $th) {
            DB::rollBack();

            return response()->json([
                'message' => 'Saving Failed',
                'error' => $th,
            ], 400);
        }
    }

    public function find(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => ['required'],
        ]);

        $target = $validator->validated();

        try {
            $pegawai = DB::table('users')
                    ->join('user_details', 'users.id', '=', 'user_details.user_id')
                    ->select('users.id', 'users.email', 'user_details.nik', 'user_details.nama', 'user_details.tanggal_lahir', 'user_details.jk', 'user_details.alamat', 'user_details.jabatan', 'user_details.no_pegawai', 'user_details.no_bpjstk')
                    ->where('users.id', '=', $target)->get();

            if ($pegawai->isEmpty()) {
                return response()->json([
                    'message' => 'User Not Found',
                ], 200);
            }

            return response()->json([
                'message' => 'success',
                'data' => $pegawai,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'Failed',
                'error' => $th,
            ], 400);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'nama' => ['required'],
            'nik' => ['required', 'min:15', 'max:18'],
            'tgl_lahir' => ['required', 'date'],
            'jk' => ['required'],
            'alamat' => ['required'],
            'jabatan' => ['required'],
            'no_pegawai' => ['required'],
            'bpjstk' => [],
        ]);

        $target = $validator->validated();

        $update = [
            'nik' => $target['nik'],
            'nama' => $target['nama'],
            'tanggal_lahir' => $target['tgl_lahir'],
            'jk' => $target['jk'],
            'alamat' => $target['alamat'],
            'jabatan' => $target['jabatan'],
            'no_pegawai' => $target['no_pegawai'],
            'no_bpjstk' => $target['bpjstk'],
        ];

        try {
            DB::beginTransaction();

            $user = User::find($id);
            $user->email = $target['email'];
            $user->save();

            UserDetail::where('user_id', $id)->update($update);

            DB::commit();

            return response()->json([
                'message' => 'Success',
            ], 200);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Failed',
                'error' => $th,
            ], 400);
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $user = User::find($id);
            $user->delete();

            $userDetail = UserDetail::where('user_id', $id);
            $userDetail->delete();

            DB::commit();

            return response()->json([
                'message' => 'Success',
            ], 200);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json([
                'message' => 'Failed',
                'error' => $th,
            ], 400);
        }
    }
}
