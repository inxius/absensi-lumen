<?php

namespace App\Http\Middleware;

use Closure;

class RolesPegawai
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->roles == 2) {
            return $next($request);
        }

        return response('Unauthorized.', 401);
    }
}
